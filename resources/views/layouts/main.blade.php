<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">

    @guest

    @else
        <div class="row">
            <div class="col-md-12">
                {{--
                                <a href="{{route('profile')}}" class="btn btn-primary my-auto float-right">Profile</a>
                --}}
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                    <input type="submit" class="btn btn-warning float-right" value="Logout">
                </form>

            </div>
        </div>
    @endguest

    <main role="main" class="py-4">


        <section class="py-5 text-center container">
            <div class="container">
                <h1 class="fw-light">Hello!</h1>

                <h2 class="fw-light">My Name is Andrii Tymoshenko I am junior PHP(Laravel) developer</h2>
                <h3 class="fw-light">and these is my skill stack</h3>

                <td><button onclick="location.href='{{ url('https://gitlab.com/Ndray/introduction') }}'">
                        The Source code u can check here</button></td>


                <div class="album py-5 bg-light">
                    <p>
                        @if(url()->current() == route('skills'))
                            <a href="#" type="button" class="btn btn-primary my-2">All skills</a>
                            <a href="{{ route('cv') }}" type="button" class="btn btn-secondary my-2">My CV</a>
                            <a href="{{ route('profile') }}" type="button" class="btn btn-secondary my-2">Edit my profile</a>
                            <a href="{{ route('weather') }}" type="button" class="btn btn-secondary my-2">Check weather in Your region</a>
                            <a href="{{ route('skills.invoicepdf') }}" type="button" class="btn btn-warning">Generate PDF with all skills </a>

                        @elseif(url()->current() == route('weather'))
                            <a href="{{ route('cv') }}" type="button" class="btn btn-secondary my-2">My CV</a>
                            <a href="#" type="button" class="btn btn-secondary my-2">Edit my profile</a>
                            <a href="{{ route('skills') }}" type="button" class="btn btn-secondary my-2">All skills</a>
                            <a href="{{ route('weather') }}" type="button" class="btn btn-primary my-2">Check weather in Your region</a>

                        @elseif(url()->current() == route('cv'))
                            <a href="#" type="button" class="btn btn-primary my-2">My CV</a>
                            <a href="{{ route('profile') }}" type="button" class="btn btn-secondary my-2">Edit my profile</a>
                            <a href="{{ route('skills') }}" type="button" class="btn btn-secondary my-2">All skills</a>
                            <a href="{{ route('weather') }}" type="button" class="btn btn-secondary my-2">Check weather in Your region</a>
                        @elseif(url()->current() == route('profile'))
                            <a href="{{ route('cv') }}" type="button" class="btn btn-secondary my-2">My CV</a>
                            <a href="#" type="button" class="btn btn-primary my-2">Edit my profile</a>
                            <a href="{{ route('skills') }}" type="button" class="btn btn-secondary my-2">All skills</a>
                            <a href="{{ route('weather') }}" type="button" class="btn btn-secondary my-2">Check weather in Your region</a>
                        @else
                            <a href="{{ route('cv') }}" type="button" class="btn btn-secondary my-2">My CV</a>
                            <a href="#" type="button" class="btn btn-secondary my-2">Edit my profile</a>
                            <a href="{{ route('skills') }}" type="button" class="btn btn-secondary my-2">All skills</a>
                            <a href="{{ route('weather') }}" type="button" class="btn btn-secondary my-2">Check weather in Your region</a>
                        @endif

                    </p>
                </div>
            </div>

        </section>
        @yield('content')
    </main>
</div>
<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">UP</a>
        </p>

    </div>
</footer>
</body>
</html>
