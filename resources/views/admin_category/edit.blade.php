@extends('adminlte::page')

@section('content')

    <form method="POST" action="{{ route('categories.update', ["category" => $category->id]) }}">
        @method('PUT')
        @csrf
        <div class="row">
            <div class="col-md-12">
                <form>
                    <div class="form-group">
                        <label >Category</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                               id="inputName"
                               value=" {{old('name') ?? $category->name }} ">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    <input type="submit" value="Save Changes" class="btn btn-outline-dark">
                </form>
                    <a href="{{ route('categories.index', ["category" => $category->all()]) }}" type="button"
                       class="btn btn-outline-danger">Back</a>


@stop
