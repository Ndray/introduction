@extends('adminlte::page')

@section('content')

    <form   method="POST" action="{{ route('categories.store') }}">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Create Category</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                           id="inputName"
                           value=" {{old('name') }} ">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
                </div>
            </div>
        </div>
 <a href="#" class="btn-btn-secondarary"> Cancel</a>
    <input type="submit" value="Create" class="btn btn-outline-dark">
    </form>

@stop

@section('js')
@stop


