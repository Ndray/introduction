@extends('adminlte::page')

@section('content')
    @include('partials.alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> </strong>
                <p class="text">
                    <strong>     Category name: </strong> {{ $category->name }} #{{ $category->id }}
                </p>

                <hr>

                <strong><i class="fa fa-map-marker margin-r-5"></i> Image</strong>

                <strong><i class="fa fa-pencil margin-r-5"></i> Time</strong>

                <p> Created at {{ $category->created_at }} </p>

                <hr>

                <strong><i class="fa fa-file-text-o margin-r-5"></i> Description</strong>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
            <a href="{{ route('categories.edit', ["category" => $category->id]) }}" type="button"
               class="btn btn-outline-info">Edit</a>
            <a href="{{ route('categories.index', ["category" => $category->all()]) }}" type="button"
               class="btn btn-outline-warning">Back to the list</a>
        </div>




@stop
