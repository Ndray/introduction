
<div>
    <ul>
    <a href="{{ route('skills') }}">
        @if(empty($categoryId))
            <b> All </b>
        @else
            All
        @endif
    </a>

        <div class="text-left">
        @foreach ($categories as $cat)
            <a href="{{ route('skills', ['category_id' => $cat->id]) }}">
                @if($cat->id == $categoryId)
                    <b>{{ $cat->name }}</b>
                @else
                    {{ $cat->name }}
                @endif
            </a>
            <br>
        @endforeach
    </div>
    </ul>
</div>

