<div class="col-4">
    <div class="card shadow-sm">
        @if(!empty($skill->image))
            <img class="bd-placeholder-img card-img-top"
                 width="100%" height="225"
                 src="{{ $skill->image }}"
                >
            <p class="card-text badge-success" >Knowledge level is: {{ $skill->level}}</p>
        @else
            <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                 xmlns="http://www.w3.org/2000/svg" role="img"
                 aria-label="Placeholder: Thumbnail"
                 preserveAspectRatio="xMidYMid slice" focusable="false"><title>
                    Placeholder</title>
                <rect width="100%" height="100%" fill="#55595c"/>
                <text x="50%" y="50%" fill="#eceeef"
                      dy=".3em">{{ $skill->name }}</text>
                <p class="card-text">Knowledge level is: {{ $skill->level}}</p>
            </svg>
        @endif
    </div>
    <br>
</div>
