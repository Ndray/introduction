@extends('layouts.main')

@section('content')


    {{-- <p class="text-center">The rate is max 10.
         <br>Where 1 - is low level and 10 - is high level</p>--}}
    <div class="container-fluid">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row" >
            <div class="col-md-2" >
                @include('skills.categories', ['categories' => $categories, 'categoryId' => $categoryId])
            </div>
            <div class="col-md-10" >
                <div class="row" >
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Skill name"
                               aria-label="Skill name" name="search" id="search" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" onclick="search()" type="button">Search</button>
                        </div>
                    </div>
                    <div class="row" id="skills" >
                        @foreach($skills as $skill)
                            @include('skills.skill', ['skill' => $skill])
                        @endforeach
                    </div>

                    {{ $skills->links() }}
                </div>
                <script>
                    function search() {
                        $("#skills").html('<div class="spinner-border>YO</div>"');
                        $(".pagination").remove();
                        const name = document.getElementById('search').value;
                        $.ajax({
                            method: "POST",
                            url: "{{ route('skills.search') }}",
                            data: { "_token": "{{ csrf_token() }}", "name": name }
                        }).done(function( msg ) {
                                $("#skills").html(msg);
                            })
                    }
                </script>
@endsection



