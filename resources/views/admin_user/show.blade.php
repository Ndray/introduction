@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <div class="col-md-12">
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> {{ $user->name }} </strong>

                <p class="text-muted">
                    ID is # {{ $user->id }}
                </p>

                <hr>

                <strong><i class="fa fa-pencil margin-r-5"></i> User Email </strong>
                <li>
                    {{ $user->email }}
                </li>
                <hr>

                <strong><i class="fa fa-pencil margin-r-5"></i> User Lang </strong>
                <li>
                    {{ $user->lang->full_name }}
                </li>
                <hr>

                <strong><i class="fa fa-pencil margin-r-5"></i> Created at </strong>

                <p> {{ $user->created_at }} </p>
                <hr>


                <strong><i class="fa fa-pencil margin-r-5"></i> Updatet at</strong>


                <p>  {{ $user->updated_at }} </p>

                <hr>

                <strong><i class="fa fa-file-text-o margin-r-5"></i> Description</strong>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
            <a href="{{ route('users.index') }}" class="btn btn-secondary">To all list</a>
            <a href="{{ route('users.edit', ["user" => $user->id]) }}" type="button" class="btn btn-info">Edit</a>
        </div>
    </div>






@stop
