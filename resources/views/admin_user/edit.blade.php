@extends('adminlte::page')

@section('content')

    <form method="POST" action="{{ route('users.update', ["user" => $user->id]) }}">
        @method('PUT')
        @csrf
        <div class="row">
            <div class="col-md-12">
                <form>
                    <div class="form-group">
                        <label for="inputName">User Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                               id="inputName"
                               value=" {{old('name') ?? $user->name }} ">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email"
                               class="form-control @error('email') is-invalid @enderror"
                               value="{{old('email') ?? $user->email }}">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="inputStatus">User Language</label>
                        <select name="lang"
                                class="form-control custom-select @error('lang') is-invalid @enderror">
                            <option value="">Choose language</option>
                            @foreach($langs as $lang)
                                <option @if(old('lang') && $lang->short_name == old('lang'))
                                        selected
                                        @elseif (empty(old('lang')) && $lang->short_name === $user->lang->short_name)
                                            selected
                                        @endif
                                        value="{{ $lang->short_name }}">{{ $lang->full_name }}</option>
                            @endforeach
                        </select>
                        @error('lang')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="inputStatus">User Status</label>
                        <select name="status"
                                class="form-control custom-select @error('status') is-invalid @enderror">
                            <option
                                @if(old('status') && "active" == old('status'))
                                selected
                                @elseif (empty(old('status')) && "active" === $user->status)
                                selected
                                @endif
                                value="active">Active </option>
                            <option
                                @if(old('status') && "not active" == old('status'))
                                selected
                                @elseif (empty(old('status')) && "not active" === $user->status)
                                selected
                                @endif
                                value="not active">Not Active </option>
                        </select>
                        @error('status')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <input type="submit" value="Save Changes" class="btn btn-outline-dark">
                </form>
                    <a href="{{ route('users.index', ["user" => $user->all()]) }}" type="button"
                       class="btn btn-outline-danger">Back</a>


@stop
