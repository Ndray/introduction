@extends('adminlte::page')

@section('content')

    <form method="POST" action="{{ route('users.store') }}">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <form>
                    <div class="form-group">
                        <label for="inputName">Create new User</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                               id="inputName"
                               value="{{old('name')}}">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email"
                               class="form-control @error('email') is-invalid @enderror"
                               value="{{old('email')  }}">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="inputStatus">User Language</label>
                        <select name="lang"
                                class="form-control custom-select @error('lang') is-invalid @enderror">
                            <option value="">Choose language</option>
                            @foreach($langs as $lang)
                                <option @if(old('lang') && $lang->short_name == old('lang'))
                                        selected
                                        @endif
                                        value="{{ $lang->short_name }}">{{ $lang->full_name }}</option>
                            @endforeach
                        </select>
                        @error('lang')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="inputStatus">User Status</label>
                        <select name="status"
                                class="form-control custom-select @error('status') is-invalid @enderror">
                            <option
                                @if(old('status') && "active" == old('status'))
                                selected
                                @endif
                                value="active">Active </option>
                            <option
                                @if(old('status') && "not active" == old('status'))
                                selected>
                                @endif
                                value="not active">Not Active </option>
                        </select>
                        @error('status')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password"
                               class="form-control @error('password') is-invalid @enderror" value="">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Confirm Password</label>
                        <input type="password" name="password_confirmation" id="password_confirmation"
                               class="form-control @error('password_confirmation') is-invalid @enderror"
                               value="">
                    </div>

                    <input type="submit" value="Create" class="btn btn-outline-dark">
                </form>
            </div>
        </div>
    </form>
{{--                    <a href="{{ route('admin.user.index', ["user" => $user->all()]) }}" type="button"--}}
{{--                       class="btn btn-outline-danger">Back</a>--}}


@stop
