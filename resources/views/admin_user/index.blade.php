@extends('adminlte::page')

@section('content')
    @include('partials.alerts')
    <table id="users" class="table table-bordered table-hover dataTable" role="grid"
           aria-describedby="example2_info">
        <thead>
        <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending"
            >Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Email
            </th>
            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending"
            >Type
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Status
            </th>
           {{-- <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >User Skills
            </th>--}}
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Language
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Actions
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr role="row">
                <td>{{ $user->name }}</td>
                <td>{{ $user->email}}</td>
                <td>{{ $user->type}}</td>
                <td>{{ $user->status}}</td>
                <td>
                    @if(!empty( $user->lang->full_name))
                        {{ $user->lang->full_name }}
                        @endif
                </td>
                <td>
                    <div class="btn-group">
                        <a href="{{ route('users.show', ["user" => $user->id]) }}" type="button"
                           class="btn btn-info">View</a>
                        <a href="{{ route('users.edit', ["user" => $user->id]) }}" type="button"
                           class="btn btn-info">Edit</a>
                        <form method="POST" action="{{ route('users.destroy', ["user" => $user->id]) }}">
                            @method('DELETE')
                            @csrf
                            <input type="submit" value="delete"  class="btn btn-info">
                        </form>
                    </div>
                </td>
        @endforeach
        </tbody>
    </table>

@stop
@section('js')
    <script>
        $(function () {
            $('#skills').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@stop
