@extends('adminlte::page')

@section('content')
    <form method="POST" action="{{ route('skills.store') }}">

        @csrf
        <div class="row">
            <div class="col-md-12">
                <form>
                    <div class="form-group">
                        <label for="name">Create Skill</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                               name="name" id="name"  value="{{ old('name') }}">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="level">Level</label>
                        <input type="number" name="level" id="level"
                               class="form-control @error('level') is-invalid @enderror" min="1"
                               value="{{ old('level') }}">
                        @error('level')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="categories">Skill Category</label>
                        <select name="category_id"
                                class="form-control custom-select @error('category_id') is-invalid @enderror">
                            <option value="">Choose category</option>
                            {{--                            <?php var_dump(old('category_id'));  ?>--}}
                            {{--                            {{ var_dump($categories[0]->id) }}--}}
                            @foreach($categories as $category)
                                <option @if(old('category_id') && $category->id == old('category_id'))
                                        selected
                                        @endif
                                        value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                        @error('category_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <a href="{{ route('skills.index') }}" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Create" class="btn btn-outline-dark">
                </form>

@endsection

