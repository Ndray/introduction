@extends('adminlte::page')

@section('content')
    @include('partials.alerts')
    <table id="skills" class="table table-bordered table-hover dataTable" role="grid"
           aria-describedby="example2_info">
        <thead>
        <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending"
            >Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Category ID
            </th>
            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending"
            >Image
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Level
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Created at
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Updated at
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Actions
        </tr>
        </thead>
        <tbody>
        @foreach($skills as $skill)
            <tr role="row">
                <td>{{ $skill->name }}</td>
                <td>{{ $skill->category_id}}</td>
                <td>{{ $skill->image}}</td>
                <td>{{ $skill->level}}</td>
                <td>{{ $skill->created_at }}</td>
                <td>{{ $skill->updated_at }}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{ route('skills.show', ["skill" => $skill->id]) }}" type="button"
                           class="btn btn-info">View</a>
                        <a href="{{ route('skills.edit', ["skill" => $skill->id]) }}" type="button"
                           class="btn btn-info">Edit</a>
                        <form method="POST" action="{{ route('skills.destroy', ["skill" => $skill->id]) }}">
                            @method('DELETE')
                            @csrf
                            <input type="submit" value="delete"  class="btn btn-info">
                        </form>
                    </div>
                </td>
        @endforeach
        </tbody>
    </table>

@stop
@section('js')
    <script>
        $(function () {
            $('#skills').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@stop
