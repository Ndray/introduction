@extends('adminlte::page')

@section('content')
    @include('partials.alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> </strong>
                <p class="text">
                    <strong>     Skill name: </strong> {{ $skills->name }}
                </p>
                <hr>
                <p class="text">
                    <strong>  Category name: </strong> {{ $skills->category->name }}
                </p>

                <hr>

                <strong><i class="fa fa-map-marker margin-r-5"></i> Image</strong>

                <p class="text-muted">{{ $skills->image }}</p>

                <hr>

                <strong><i class="fa fa-pencil margin-r-5"></i> Level is:  {{ $skills->level }}</strong>
                <hr>


                <strong><i class="fa fa-pencil margin-r-5"></i> Time</strong>

                <p> Created at {{ $skills->created_at }} </p>
                <p> Updatet at {{ $skills->updated_at }} </p>

                <hr>

                <strong><i class="fa fa-file-text-o margin-r-5"></i> Description</strong>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
            <a href="{{ route('skills.edit', ["skill" => $skills->id]) }}" type="button"
               class="btn btn-outline-info">Edit</a>
            <a href="{{ route('skills.index', ["skill" => $skills->all()]) }}" type="button"
               class="btn btn-outline-warning">Back to the list</a>
        </div>




@stop
