<?php

namespace Database\Seeders;

use App\Models\Skill;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->initSkills();
    }

    private function initSkills(): self
    {
        Skill::create(['name' => 'apache']);
        Skill::create(['name' => 'api']);
        Skill::create(['name' => 'backpack']);
        Skill::create(['name' => 'bootstrap']);
        Skill::create(['name' => 'english']);
        Skill::create(['name' => 'laravel']);
        Skill::create(['name' => 'laravel auth']);
        Skill::create(['name' => 'linux']);
        Skill::create(['name' => 'mvc']);
        Skill::create(['name' => 'mysql']);
        Skill::create(['name' => 'oop']);
        Skill::create(['name' => 'git']);
        //Skill::create(['category_id' => rand(1,10)]);

        return $this;
    }
}
