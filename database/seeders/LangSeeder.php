<?php

namespace Database\Seeders;

use App\Models\Lang;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LangSeeder extends Seeder
{
    private $langs = ["UA" => "Ukrainian", "DE" => "German", "EN" => "English"];

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->langs as $code => $lang) {
            Lang::create(
                [
                    "full_name" => $lang,
                    "short_name" => $code
                ]
            );
        }
    }
}
