<?php


namespace App\Services;

use App\Repositories\UserRepository;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Services\LangService;
use Illuminate\Support\Facades\Hash;


class UserService extends BaseService
{
    public function __construct(UserRepository $repo, LangService $langs)
    {
        $this->repo = $repo;
        $this->langs = $langs;

    }

    public function getWeatherWithCurrentUserLocation()
    {
        return $this->repo->currentWeather();
    }

    public function activeUsersCount(): int
    {
        return $this->repo->activeUsersCount();
    }

    public function getNotAdminUsers()
    {
        return $this->repo->getNotAdminUsers();
    }

    public function update($id, array $data): bool
    {
        if (!empty($data['lang'])) {
            $langId = $this->langs->getIdByShortName($data['lang']);
            $data['lang_id'] = $langId;
        } else {
            $data['lang_id'] = null;
        }
        unset($data['lang']);
        return parent::update($id, $data);
    }

    /**
     * Create new record
     *
     * @param array $input
     * @return model
     */
    public function create(array $data)
    {
        if (!empty($data['lang'])) {
            $langId = $this->langs->getIdByShortName($data['lang']);
            $data['lang_id'] = $langId;
        }
        unset($data['lang']);
        $data['password'] = Hash::make($data['password']);
        $data['type'] = 'user';
        return parent::create($data);
    }


}
