<?php


namespace App\Services;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class BaseService
{

    /**
     * Repository
     *
     * @var Repository
     */
    public $repo;

    /**
     * Get all data
     *
     * @return Collection
     */
    public function all()
    {
        return $this->repo->all();
    }

    /**
     * Get all data
     *
     * @return Collection
     */
    public function getAllWithPagination()
    {
        return $this->repo->allWithPagination();
    }

    /**
     * Create new record
     *
     * @param array $input
     * @return model
     */
    public function create(array $data)
    {
        return $this->repo->create($data);
    }

    /**
     * Find record by id
     *
     * @param int $id
     * @return Model
     */
    public function findById($id)
    {
        return $this->repo->findById($id);
    }

    /**
     *
     * @param integer $id
     * @param array @data
     * @return boolean
     */
    public function update(string $id, array $data): bool
    {
        return $this->repo->update($id, $data);
    }

    /**
     * Delete record by id
     *
     * @param integer $id
     * @return boolean
     */
    public function destroy(string $id): bool
    {
        return $this->repo->destroy($id);
    }

    /**
     * Count records
     *
     * @param integer
     */
    public function count(): int
    {
        return $this->repo->count();
    }


    public function getWeatherWithCurrentUserLocation()
    {
        $this->repo->currentWeather();
    }
}
