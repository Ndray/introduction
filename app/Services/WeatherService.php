<?php


namespace App\Services;

use App\Repositories\WeatherRepository;

class WeatherService extends BaseService
{
    public function __construct(WeatherRepository $repo)
    {
        $this->repo = $repo;

    }

    public function getWeatherWithCurrentUserLocation()
    {
        return $this->repo->currentWeather();
    }
}
