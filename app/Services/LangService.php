<?php


namespace App\Services;

use App\Models\Lang;
use App\Services\BaseService;
use App\Repositories\LangRepository;
use Illuminate\Database\Eloquent\Model;

class LangService extends BaseService
{
    public function __construct(LangRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getIdByShortName(string $shortName): string
    {
        $lang = $this->repo->getByShortName($shortName);
        return $lang->id;
    }
}
