<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\SkillRepository;
use Illuminate\Pagination\LengthAwarePaginator;


class SkillService extends BaseService
{
    public function __construct(SkillRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getByCategoryWithPagination(string $categoryId): LengthAwarePaginator
    {
        return $this->repo->getByCategoryWithPagination($categoryId);
    }


    public function search(string $name)
    {
        return $this->repo->search($name);
    }
}
