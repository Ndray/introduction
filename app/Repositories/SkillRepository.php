<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Skill;
use Illuminate\Support\Collection;


class SkillRepository extends BaseRepository
{
    public function __construct(Skill $model)
    {
        $this->model = $model;
    }

    public function getByCategoryWithPagination($categoryId)
    {
        return $this->model->where("category_id", $categoryId)->paginate($this->count);
    }

    public function search($name): Collection
    {
        return $this->model->where("name", 'like', '%' . $name. '%')->get();
    }
}
