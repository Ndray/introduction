<?php


namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\Interfaces\BaseInterface;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class BaseRepository implements BaseInterface
{

    protected $model;

    public $sortBy = 'id';
    public $sortOrder = 'desc';
    public $count = 5;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function currentWeather()
    {
        return $this->model;
    }

    public function all(): Collection
    {
        return $this->model->query()
            ->orderBy($this->sortBy, $this->sortOrder)
            ->get();
    }

    public function create(array $data)
    {
        return $this->model->query()->create($data);
    }

    public function update(int $id, array $data): bool
    {
        $query = $this->model->where('id', $id);
        return $query->update($data);
    }

    public function destroy(int $id): bool
    {
        $this->model->destroy($id);
        return true;
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model|null
     */
    public function findById($id)
    {
        return $this->model->query()->find($id);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel(Model $model)
    {
        $this->model = $model;
        return true;
    }

    public function count(): int
    {
        return $this->model->query()->count();
    }

    public function allWithPagination()
    {
        return $this->model->paginate($this->count);
    }

}
