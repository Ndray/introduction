<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Lang;


class LangRepository extends BaseRepository
{
    public function __construct(Lang $model)
    {
        $this->model = $model;
    }

    public function getByShortName(string $shortName): Lang
    {
        return $this->model->where('short_name', $shortName)->first();
    }
}
