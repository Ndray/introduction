<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


interface BaseInterface
{
    public function currentWeather();

    public function all();

    public function create(array $data);

    public function update(int $id, array $data);

    public function destroy(int $id);

    public function findById(int $id);

    public function count();
}
