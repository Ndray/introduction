<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Collection;

class UserRepository extends BaseRepository
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }


    public function activeUsersCount(): int
    {
        return $this->model->where("type", "user")->where("status", "active")->count();
    }

    public function getNotAdminUsers()
    {
        return $this->model->where("type", "<>", "admin")->get();
    }


}

