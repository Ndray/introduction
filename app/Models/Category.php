<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Skill;
use Carbon\Carbon;

class Category extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    protected $fillable = ['name'];

    public function skills()
    {
        return $this->hasMany(Skill::class, 'category_id', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return date("Y-m-d", strtotime($value));
    }

}
