<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skill extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'skills';

    /**
     * The table associated with the model.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'level',
        'category_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'users');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return date("Y-m-d", strtotime($value));
    }

    public function getUpdatedAtAttribute($value)
    {
        return date("Y-m-d", strtotime($value));
    }


}
