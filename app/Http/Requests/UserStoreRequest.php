<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules()
    {
        return [
            'name' => [
                "required",
                "max:255",
                "min:3",
            ],
            'email' => [
                "required",
                "max:255",
                "email",
                "unique:users,email"

            ],
            'level' => 'required|min:1|max:5',
            'status' => 'required|in:active,not active',
            'lang' => 'nullable|exists:langs,short_name',
            'password' => 'required|min:8|confirmed'

        ];
    }
}
