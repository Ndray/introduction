<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SkillStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */

     public function rules(): array
     {
         return [
             'name' => [
                 "required",
                 "max:255",
                 "min:3",
                 Rule::unique('skills')
                     ->where('category_id', $this->category_id)
             ],
             'level' => 'required|min:1|max:5',
             'category_id' => 'required|integer|exists:categories,id'
         ];
     }
}
