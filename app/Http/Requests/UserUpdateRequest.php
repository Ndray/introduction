<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * @var mixed
     */
    private $user;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules()
    {
        return [
            'name' => [
                "required",
                "max:255",
                "min:3",
            ],
            'email' => [
                "required",
                "max:255",
                "email",
                Rule::unique('users')
                    ->ignore($this->user)
            ],
            'level' => 'required|min:1|max:5',
            'status' => 'required|in:active,not active',
            'lang' => 'nullable|exists:langs,short_name'

        ];
    }
}
