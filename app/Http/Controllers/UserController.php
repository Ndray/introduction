<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\Models\Lang;
use App\Models\User;
use App\Services\WeatherService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use App\Services\UserService;
use App\Http\Requests\UpdateUserProfileRequest;
use App\Services\LangService;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @param LangService $lang
     * @param UserService $user
     */
    public function __construct(LangService $lang, UserService $user)
    {
        $this->user = $user;
        $this->lang = $lang;

    }

    public function profile(Request $request)
    {
//        dd(Auth::user()->lang);
        return view('user.profile',
            [
                "user" => Auth::user(),
                "langs" => $this->lang->all(),
            ]
        );
    }

    public function update(UpdateUserProfileRequest $request)
    {
    $user = Auth::user();
    $user->name = $request->input('name');
    $user->lang_id = Lang::where('short_name', $request->input('lang'))->value('id');
    $user->save();
        /*$this->user->update(
            Auth::id(),
            [
                "name" => $request->input('name'),
                "lang_id" => $this->lang->getIdByShortName($request->input('lang')),
            ]
        );*/
        return redirect()->route('profile');
    }



    public function googleRedirect()
    {
        return Socialite::driver('google')->redirect();
    }


    public function loginWithGoogle()
    {
        try {
            $user = Socialite::driver('google')->user();
            $isUser = User::where('google_id', $user->id)->first();

            // если такой пользователь есть, значит авторизуемся
            //иначе регистрируем

            if ($isUser) {
                Auth::login($isUser);
                return redirect('/login');
            } else {
                $createUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id' => $user->id,
                    'password' => encrypt('user'),
                ]);

                Auth::login($createUser);

                return redirect('/login');
            }
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
