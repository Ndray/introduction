<?php

namespace App\Http\Controllers;

use App\Models\Skill;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use App\Services\SkillService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Mpdf\Mpdf;


class SkillController extends Controller
{
    /**
     * @var SkillService
     */
    private $skills;

    /**
     * @var CategoryService
     */
    private $categories;

    public function __construct(SkillService $skills, CategoryService $categories)
    {
        $this->skills = $skills;
        $this->categories = $categories;
    }

    public function index(Request $request, $categoryId = null)
    {
        if (empty($categoryId)) {
            $skills = $this->skills->getAllWithPagination();
        } else {
            $skills = $this->skills->getByCategoryWithPagination($categoryId);
        }
        //$skills = $this->skills->getAllWithPagination();
        $categories = $this->categories->all();

        return  view('skills.index', ["skills" => $skills, "categories" => $categories, "categoryId" => $categoryId]);
    }

    public function search(Request $request)
    {
        $skill = $this->skills->search($request->name);
        $view = View::make(
            'skills.search',
            [
                "skills" => $this->skills->search($request->name),
            ]
        );
        return $view->render();
    }

    public function invoicePdf(Request $request, Skill $skill)
    {
        /** @var Skill $skill */
        $skill = $this->skills->all();
        $view = View::make('skills.invoicepdf',
            [
                "skills" => $this->skills->all(),
            ]
        );
        $html = $view->render();
        $mpdf = new Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output("invoice.pdf", "D");
    }

    public function showCV(Request $request)
    {
        $skills = $this->skills->all();
        return view('skills.cv', ["skills" => $skills]);
    }
}
