<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserProfileRequest;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use App\Models\UserSkill;
use App\Services\LangService;
use App\Services\SkillService;
use App\Services\UserSkillService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Services\UserService;


class AdminUserController extends Controller
{

    /**
     * @var UserService
     */
    private $users;

    /**
     * @var LangService
     */
    private $langs;

    /**
     * @var SkillService
     */
    private $skills;

    /**
     * @var UserSkillService
     */
    private $userSkills;

    public function __construct(UserService $users, LangService $langs, SkillService $skills, UserSkillService $userSkills )
    {
        $this->users = $users;
        $this->langs = $langs;
        $this->skills = $skills;
        $this->userSkills = $userSkills;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        /** @var UserSkill $userSkills */
        return view('admin_user.index', [
            "users" => $this->users->getNotAdminUsers(),
            "skills" => $this->userSkills->all(),
        ]);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
       return view('admin_user.create',
        [
            "langs" => $this->langs->all(),
            "skills" => $this->skills->all(),
        ]
    );
    }

    /**
     * Store a newly created resource in storage.
     * @param UserStoreRequest $request
     */
    public function store(UserStoreRequest $request) :RedirectResponse
    {
        /** @var User $user */
        $user = $this->users->create($request->all());

        return redirect()->route('users.show', ["user" => $user])->withSuccess('User #' . $user->id . 'was created!');
    }

    /**
     * Display the specified resource.
     * @param $id
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function show($id)
    {
        return view('admin_user.show',
            [
                "user" => $this->users->findById($id)
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function edit($id)
    {
        return view('admin_user.edit',
            [

                "user" => $this->users->findById($id),
                "langs" => $this->langs->all(),
                "skills" => $this->skills->all(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param User $user
     * @return
     */
    public function update(Request $request, User $user)
    {
        //dd($user->id);
        $this->users->update($user->id, $request->except(['_method', '_token']));
        return redirect()->route('users.show', ["user" => $user->id])->withSuccess('User #' . $user->id . 'was updated!');
    }

    /**
     * Remove the specified resource from storage.
     * @param string $id
     */
    public function destroy($id)
    {
        $this->users->destroy($id);
        return redirect()->route('users.index')->withSuccess('User #' . $id . 'was deleted!');
    }
}
