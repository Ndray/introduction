<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Services\CategoryService;
use App\Http\Requests\CategoryUpdateRequest;
use App\Http\Requests\CategoryStoreRequest;
use Illuminate\Validation\Rule;

class AdminCategoryController extends Controller
{
    /**
     * @var CategoryService
     */
    private $categories;

    public function __construct(CategoryService $categories)
    {
        $this->categories = $categories;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin_category.index', ["categories" => $this->categories->all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin_category.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     */
    public function store(CategoryStoreRequest $request)
    {

        $category = $this->categories->create($request->all());
        return redirect()->route('categories.show', ["category" => $category->id])->withSuccess('Category #' . $category->id . " " . 'was created!');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        return view('admin_category.show',
        ["category" => $this->categories->findById($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        return view('admin_category.edit',
            [

                "category" => $this->categories->findById($id),
                ]);
    }

    /**
     * Update the specified resource in storage.
     * @param CategoryUpdateRequest $request
     * @param Category $category
     * @return
     */
    public function update(CategoryUpdateRequest $request, Category $category)
    {
        $this->categories->update($category->id, $request->except(['_method', '_token']));
        return redirect()->route('categories.show', ["category" => $category->id])->withSuccess('Category #' . $category->id . 'was updated!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $this->categories->destroy($id);
        return redirect()->route('categories.index')->withSuccess('Category #' . $id . 'was deleted!');
    }
}
