<?php

namespace App\Http\Controllers;

use App\Models\Skill;
use App\Services\CategoryService;
use App\Services\SkillService;
use App\Http\Requests\SkillUpdateRequest;
use App\Http\Requests\SkillStoreRequest;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;


class AdminSkillController extends Controller
{

    /**
     * @var SkillService
     */
    private $skills;
    /**
     * @var CategoryService
     */
    private $categories;

    public function __construct(SkillService $skills, CategoryService $categories)
    {
        $this->skills = $skills;
        $this->categories = $categories;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin_skill.index', ["skills" => $this->skills->all()
        ]);
    }
    //

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin_skill.create',
            [

                "categories" => $this->categories->all(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param SkillStoreRequest $request
     */
    public function store(SkillStoreRequest $request)
    {
        $skill = $this->skills->create($request->all());
        return redirect()->route('skills.show', ["skill" => $skill->id])->withSuccess('Skill #' . $skill->id . " " . 'was created!');
    }

    /**
     * Display the specified resource.
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View|Application
     */
    public function show($id)
    {
        return view('admin_skill.show',
            ["skills" => $this->skills->findById($id)
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View|Application
     */
    public function edit($id)
    {
        return view('admin_skill.edit',
            [

                "skills" => $this->skills->findById($id),
                "categories" => $this->categories->all(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     * @param SkillUpdateRequest $request
     * @param Skill $skill
     * @return
     */
    public function update(SkillUpdateRequest $request, Skill $skill)
    {


        $this->skills->update($skill->id, $request->except(['_method', '_token']));
        return redirect()->route('skills.show', ["skill" => $skill->id])->withSuccess('Skill #' . $skill->id . 'was updated!');
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param Skill $skill
     * @return
     */
    public function destroy($id)
    {
        $this->skills->destroy($id);
        return redirect()->route('skills.index')->withSuccess('Skill #' . $id . 'was deleted!');
    }
}
