<?php

namespace App\Http\Controllers;

use App\Services\CategoryService;
use App\Services\SkillService;
use App\Services\UserService;
use Illuminate\Http\Request;

class AdminMainController extends Controller
{

    public function __construct(SkillService $skills, UserService $users, CategoryService $categories)
    {
        $this->skills = $skills;
        $this->users = $users;
        $this->categories = $categories;
    }

    public function index(Request $request)
    {

        return view('admin.admin_main',
            [
                "skillsCount" => $this->skills->count(),
                "categoriesCount" => $this->categories->count(),
                "usersCount" => $this->users->activeUsersCount(),
            ]
        );
    }
}
