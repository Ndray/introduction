<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;

class WeatherController extends Controller
{
    /**
     * @var UserService
     */
    private $user;

    /**
     * Create a new controller instance.
     *
     * @param UserService $user
     */
    public function __construct(UserService $user)
    {
        $this->user = $user;
    }

    /**
     * Show the application dashboard.
     * 59+
     *
     * @return \Illuminate\Database\Eloquent\Model|void
     */
    public function currentWeather()
    {
        return $this->user->getWeatherWithCurrentUserLocation();
    }
}
