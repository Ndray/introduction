<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AdminCategoryController;
use App\Http\Controllers\AdminSkillController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SkillController;
use App\Http\Controllers\WeatherController;
use App\Http\Controllers\AdminMainController;
use App\Http\Controllers\AdminUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Auth::routes(['verify' => true]);

Route::get('/', function () {
    return view('welcome');
});


Route::middleware(['auth', 'user.check'])->group(function () {
    Route::prefix('profile')->group(function () {
        Route::get('/auth/google', [UserController::class, 'googleRedirect'])->name('auth.google');
        Route::get('/auth/google/callback', [UserController::class, 'loginWithGoogle']);
        Route::get('/', [UserController::class, 'profile'])->name('profile');
        Route::post('/update', [UserController::class, 'update'])->name('profile.update');
    });
    Route::get('/home{category_id?}', [SkillController::class, 'index'])->name('skills')->where('category_id', '[0-9]+');
    Route::post('/skills/search', [SkillController::class, 'search'])->name('skills.search');
    Route::get('/weather', [WeatherController::class, 'currentWeather'])->name('weather');
    Route::get('/cv', [SkillController::class, 'showCV'])->name('cv');
    Route::get('/skills/invoice_pdf', [SkillController::class, 'invoicePdf'])
        ->name('skills.invoicepdf');
    //Route::get('/admin', [App\Http\Controllers\SkillController::class, 'index'])->name('admin')->middleware('auth.type:user');

});
Route::middleware(['auth', 'admin.check'])->group(function () {
    Route::prefix('admin')->group(
        function () {
            Route::get('/', [AdminMainController::class, 'index'])->name('admin');
            Route::resource('skills', AdminSkillController::class);
            Route::resource('categories', AdminCategoryController::class);
            Route::resource('users', AdminUserController::class);
        });
});


//Route::get('/home',[ \App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
//Route::get('/admin',[ \App\Http\Controllers\HomeController::class, 'admin'])->name('admin')->middleware('auth')


//        Route::post('/', [\App\Http\Controllers\UserController::class, 'update'])->name('profile.update');
//        Route::get('/user', [\App\Http\Controllers\UserController::class, 'show'])->name('user.show');
//        Route::get('/invoice_pdf', [\App\Http\Controllers\UserController::class, 'invoicePdf'])
//            ->name('user.invoicepdf');
